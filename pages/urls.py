
from django.urls import path, include
from . import views
from django.contrib.auth import views as auth_view
urlpatterns = [
    
    #Property Url
    path('',views.home.as_view(), name='home'),
    path('create', views.PropertyCreateView.as_view(), name="property_create"),
    path('edit/<int:pk>', views.PropertyUpdateView.as_view(), name="property_update"),
    path('detail/<int:pk>', views.PropertyDetailView.as_view(), name="property_detail"),
    path('delete/<int:pk>', views.PropertyDelete, name="property_delete"),
    
]
